FROM ubuntu:bionic
ENV DEBIAN_FRONTEND noninteractive

RUN apt-get update && apt-get install -y \
    cmake \
    clang \
    python3-setuptools

ADD https://github.com/keystone-engine/keystone/archive/master.tar.gz /keystone.tgz
RUN tar xvf /keystone.tgz && mv /keystone-master /keystone
RUN mkdir /keystone/build/
WORKDIR /keystone/build/
RUN ../make-share.sh
WORKDIR /keystone/bindings/python/
RUN make install3
RUN cp /keystone/build/llvm/lib/libkeystone.so /usr/local/lib/python3.6/dist-packages/keystone/

WORKDIR /
